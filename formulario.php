<html>
<head>
    <title>Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/stylesInfo.css">
</head>
    <body>
    <nav id="menu">
        <ul>
            <li><a href="info.php">Home</a></li>
            <li><a href="formulario.php">Registar Alumnos</a></li>
            <li><a href="login.php">Cerrar Sesion</a></li>
        </ul>
    </nav>

	<div class="container2">
		<div class="columns2">
			<!--
				En la etiqueta form son importantes los siguientes atributos method y action
				method para especificar el método en que se va a enviar el formulario "get" o "post" cambien el tipo de metodo para ver cambios
				action para especificar el archivo que va a procesar la información
			-->
			<form class="formulario" action="procesar_formulario.php?accion=get&texto=textoenget" method="POST">
				
                <label class="form-label" for="input-text">Número de Cuenta</label>
                <input name="numCuenta" class="form-input " type="text" id="input-nombre" placeholder="Número de Cuenta">
				<label class="form-label" for="input-text">Nombre</label>
				<input name="nombre" class="form-input " type="text" id="input-nombre" placeholder="Nombre">
                <label class="form-label" for="input-text">Apellido Paterno</label>
                <input name="aPaterno" class="form-input " type="text" id="input-nombre" placeholder="Apellido Paterno">
                <label class="form-label" for="input-text">Apellido Materno</label>
                <input name="aMaterno" class="form-input " type="text" id="input-nombre" placeholder="Apellido Paterno">

				<!-- form radio control -->
				<label class="form-label">Sexo</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="H" checked>
					<i class="form-icon"></i> Hombre
				</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="M">
					<i class="form-icon"></i> Mujer
				</label>

				<!-- HTML 5 controls -->
				<!-- form date control -->
				<label class="form-label" for="input-date">Fecha</label>
				<input name="date" class="form-input " type="date" id="input-date"
					   placeholder="fecha">

                <!-- form password control -->
				<label class="form-label" for="input-password">Contraseña</label>
				<input name="password" class="form-input" type="password" id="input-password"
					   placeholder="Contraseña">

				<!-- Botones -->
				<input type='submit' class="btn" value="Enviar"/>
				<input type='reset' class="btn btn-primary" value="limpiar"/>
			</form>
		</div>
	</div>
    </body>
</html>