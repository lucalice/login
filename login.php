<html>
<head>
    <title>Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    -->
    <link rel="stylesheet" href="./css/styles.css">
</head>
    <body>
	<div class="container">
		<div class="columns">
			<!--
				En la etiqueta form son importantes los siguientes atributos method y action
				method para especificar el método en que se va a enviar el formulario "get" o "post" cambien el tipo de metodo para ver cambios
				action para especificar el archivo que va a procesar la información
			-->
			<form action="info.php?accion=get&texto=textoenget" method="POST">
                <h1 class="titulo">Login</h1>
                <!-- form text control -->
				<label class="form-label" for="input-text">Número de cuenta</label>
				<!--
					Cuando se procesa en PHP el formulario este los transforma en un arreglo asociativo
					donde la llave sera el name del input y el valor seta el valor del input
				-->
				<input name="texto" class="form-input " type="text" id="input-nombre" placeholder="Ej. 315286512">
				<!-- form password control -->
				<label class="form-label" for="input-password">Contraseña</label>
				<input name="password" class="form-input" type="password" id="input-password"
					   placeholder="Contraseña">

				<!-- form date control 
				<label class="form-label" for="input-email">Contraseña</label>
				<input name="email" class="form-input " type="password" id="input-date"
					   placeholder="Correo">
                -->
				<!-- Botones -->
				<input type='submit' class="btn" value="Entrar"/>
			</form>
		</div>
	</div>
    </body>
</html>