<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/stylesInfo.css">
    <title>Información</title>
</head>
<body>
    <nav id="menu">
        <ul>
            <li><a href="info.php">Home</a></li>
            <li><a href="formulario.php">Registar Alumnos</a></li>
            <li><a href="login.php">Cerrar Sesion</a></li>
        </ul>
    </nav>
    <div id="container">
        <div id="info_usuario">
            <ul>
                <li id="encabezado"><a>Información usuario autenticado: </a></li>
                <li> 
                    <?php
                        echo "<p> Número de cuenta: ";
                        print_r($_POST['texto']);
                        echo "</p>";
                    ?>
                </li>
                <li>
                    Fecha de nacimiento: 
                </li>
            </ul>
        </div>

        <div id="info_usuario">
            <!-- Aquí es donde debemos ir agregando a los nuevos registros -->
            <ul class="registros">
                <li>#</li>
                <li>Nombre</li>
                <li>Fecha de nacimiento</li>
            </ul>
            <ul class="registros">
                <li>1</li>
                <li>Luis Enrique Carranza Escobar</li>
                <li>06/01/1999</li>
            </ul>
            <ul class="registros">
                <li>1</li>
                <li>Luis Enrique Carranza Escobar</li>
                <li>06/01/1999</li>
            </ul>
            <ul class="registros">
                <li>1</li>
                <li>Luis Enrique Carranza Escobar</li>
                <li>06/01/1999</li>
            </ul>
        </div>
    </div>
</body>
</html>